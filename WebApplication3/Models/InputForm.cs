﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication3.Models
{
    public class InputForm
    {
        public string FormUsername{ get; set; }
        public string FormPassword{ get; set; }
    }
}